<?php
require_once 'Poisson.php';

class MatchForecast
{
    private $historyResults;

    public function __construct($historyResults)
    {
        $this->historyResults = $historyResults;
    }

    public function getForecast($c1, $c2)
    {
        if(isset($this->historyResults[$c1]) && isset($this->historyResults[$c2])){
            $winPowerC1 = $this->historyResults[$c1]['goals']['scored'] / $this->historyResults[$c1]['games'];
            $winPowerC2 = $this->historyResults[$c2]['goals']['scored'] / $this->historyResults[$c2]['games'];

            $poisson = new \Poisson\Poisson();

            $winC1 = 0;
            $winC2 = 0;

            for($i = 1; $i < 6; $i++){
                $winC1 += $poisson->percentage($winPowerC1, $i);
                $winC2 += $poisson->percentage($winPowerC2, $i);
            }

            //средняя вероятность на выигрышь
            $winC1 = $winC1 / 5;
            $winC2 = $winC2 / 5;

            $score1 = mt_rand(0, 100);
            if($winC1 > $winC2){
                $goalsC1 = (int)floor($score1 / 10);
                $goalsC2 = (int)floor(mt_rand(0, (int) $winC2*100/$winC1) / 10);
            }else{
                $goalsC2 = (int)floor($score1 / 10);
                $goalsC1 = (int)floor( mt_rand(0, (int) $winC1*100/$winC2) / 10);
            }

            return array($goalsC1, $goalsC2);

        }else
            return false;
    }
}